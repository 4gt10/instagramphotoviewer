//
//  LoginViewController.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = AppString(InstagramPhotoViewer)
        let reloadItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "refresh")
        self.navigationItem.rightBarButtonItem = reloadItem
        
        self.webView.scrollView.scrollEnabled = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadUserProfile()
    }
    
    // MARK: - Web view
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        var result = true
        if let accessToken = self.getAccessTokenFromURL(request.URL) {
            SessionManager.sharedManager.login(accessToken: accessToken)
            InstagramManager.sharedManager.getUserProfile(
                successed: { user in
                    dispatch_async(dispatch_get_main_queue()) {
                        SessionManager.sharedManager.session?.user = user
                        self.stopLoadingAnimations()
                        
                        self.performSegueWithIdentifier(SegueID.LoginToUserMedia, sender: self)
                    }
                },
                failed: { error in
                    dispatch_async(dispatch_get_main_queue()) {
                        ModalUtils.displayErrorAlert(
                            message: error.localizedDescription,
                            parentController: self
                        )
                        self.stopLoadingAnimations()
                    }
                }
            )
            result = false
        }
        return result
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        self.startLoadingAnimations()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.stopLoadingAnimations()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        if let error = error {
            if (error.code != 102) {
                ModalUtils.displayErrorAlert(
                    message: error.localizedDescription,
                    parentController: self
                )
                self.stopLoadingAnimations()
            }
        }
    }
    
    // MARK: - Actions
    
    func refresh() {
        self.requestInstagramLoginPage()
    }
    
    // MARK: - Helpers
    
    private func loadUserProfile() {
        if let session = SessionManager.sharedManager.session {
            let accessToken = session.accessToken!
            InstagramManager.sharedManager.getUserProfile(
                successed: { user in
                    dispatch_async(dispatch_get_main_queue()) {
                        SessionManager.sharedManager.login(accessToken: accessToken)
                        SessionManager.sharedManager.session?.user = user
                        self.stopLoadingAnimations()
                        
                        self.performSegueWithIdentifier(SegueID.LoginToUserMedia, sender: self)
                    }
                },
                failed: { error in
                    dispatch_async(dispatch_get_main_queue()) {
                        ModalUtils.displayErrorAlert(
                            message: error.localizedDescription,
                            parentController: self
                        )
                        self.stopLoadingAnimations()
                    }
                }
            )
            self.startLoadingAnimations()
        } else {
            self.requestInstagramLoginPage()
        }
    }
    
    private func requestInstagramLoginPage() {
        let requestURL = NSURL(string: InstagramURL.Authorization)!
        let request = NSURLRequest(URL: requestURL)
        self.webView.loadRequest(request)
    }
    
    private func startLoadingAnimations() {
        self.loadingView.startAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    private func stopLoadingAnimations() {
        self.loadingView.stopAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    private func getAccessTokenFromURL(URL: NSURL?) -> String? {
        let AccessTokenKey = "access_token="
        
        var accessToken: String?
        if let urlString = URL?.absoluteString {
            if (urlString.containsString(AccessTokenKey)) {
                if let accessTokenRange = urlString.rangeOfString(AccessTokenKey) {
                    accessToken = urlString.substringFromIndex(accessTokenRange.endIndex)
                }
            }
        }
        return accessToken
    }
}
