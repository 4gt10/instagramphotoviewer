//
//  PictureViewerViewController.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 31.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class PictureViewerViewController: UIViewController, UIPageViewControllerDataSource {
    
    var pageViewController = UIPageViewController(
        transitionStyle: .Scroll,
        navigationOrientation: .Horizontal,
        options: nil
    )
    
    var picture: Picture? {
        didSet {
            if let picture = self.picture {
                self.currentPictureIndex = (self.pictures as NSArray).indexOfObject(picture)
            }
        }
    }
    var pictures: [Picture] = []
    var currentPictureIndex: Int = 0
    var loading = false {
        didSet {
            self.updateUI()
        }
    }
    var canLoadMore = true
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageViewController.dataSource = self
        if let startingViewController = self.viewControllerForIndex(self.currentPictureIndex) {
            let viewControllers = [startingViewController]
            self.pageViewController.setViewControllers(
                viewControllers,
                direction: .Forward,
                animated: false,
                completion: nil
            )
        }
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "comments"),
            style: .Plain,
            target: self,
            action: "displayComments"
        )
        
        self.updateUI()
    }
    
    // MARK: - Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == SegueID.PictureViewerToComments) {
            if let commentsVC = segue.destinationViewController as? PictureCommentsViewController {
                let picture = self.pictures[self.currentPictureIndex]
                commentsVC.picture = picture
            }
        }
    }
    
    // MARK: - UI
    
    func updateUI() {
        if (self.loading) {
            self.title = AppString(Loading) + "..."
        } else {
            if (!self.pictures.isEmpty) {
                self.title = "\(self.currentPictureIndex + 1) " + AppString(Of) + " \(self.pictures.count)"
            } else {
                self.title = AppString(NoPhoto)
            }
        }
    }
    
    // MARK: - Page view controller
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let previousIndex = (viewController as! PictureViewController).index - 1
        return self.viewControllerForIndex(previousIndex)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let nextIndex = (viewController as! PictureViewController).index + 1
        if (nextIndex >= self.pictures.count) {
            if (!self.loading && self.canLoadMore) {
                let picture = self.pictures[self.currentPictureIndex]
                InstagramManager.sharedManager.getUserMedia(
                    maxId: picture.id,
                    successed: { userMedia, canLoadMore in
                        dispatch_async(dispatch_get_main_queue()) {
                            self.pictures.appendContentsOf(userMedia)
                            self.canLoadMore = canLoadMore
                            
                            // Reload current view to make available paging to the new loaded view
                            if let currentVC = self.viewControllerForIndex(self.currentPictureIndex) {
                                self.pageViewController.setViewControllers([currentVC], direction: .Forward, animated: false, completion: nil)
                            }
                            
                            // Notify UserMediaViewController that we loaded more pictures
                            NSNotificationCenter.defaultCenter().postNotificationName(
                                NotificationCenter.UserMediaLoaded,
                                object: nil,
                                userInfo: [NotificationKey.UserMedia : userMedia, NotificationKey.CanLoadMore : canLoadMore]
                            )
                            
                            self.loading = false
                        }
                    },
                    failed: { error in
                        dispatch_async(dispatch_get_main_queue()) {
                            ModalUtils.displayErrorAlert(
                                message: error.localizedDescription,
                                parentController: self
                            )
                            self.loading = false
                        }
                    }
                )
                self.loading = true
            }
            return nil
        }
        return self.viewControllerForIndex(nextIndex)
    }
    
    // MARK: - Actions
    
    func displayComments() {
        self.performSegueWithIdentifier(SegueID.PictureViewerToComments, sender: nil)
    }
    
    // MARK: - Helpers
    
    private func viewControllerForIndex(index: Int) -> PictureViewController? {
        var result: PictureViewController?
        if (index >= 0) {
            if (index < self.pictures.count) {
                if let pictureVC = self.storyboard?.instantiateViewControllerWithIdentifier(ViewControllerID.PictureVC) as? PictureViewController {
                    pictureVC.parent = self
                    pictureVC.index = index
                    pictureVC.picture = self.pictures[index]
                    result = pictureVC
                }
            }
        }
        return result
    }
}
