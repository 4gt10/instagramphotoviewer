//
//  PictureViewController.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 31.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit
import SDWebImage

class PictureViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var parent: PictureViewerViewController?
    var index: Int = 0
    var picture: Picture?
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.sd_setImageWithURL(NSURL(string: self.picture!.pictureURL!)!)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let parent = self.parent {
            parent.currentPictureIndex = self.index
            parent.updateUI()
        }
    }
    
    // MARK: - Scroll view
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
