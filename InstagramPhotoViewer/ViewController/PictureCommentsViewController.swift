//
//  PictureCommentsViewController.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 31.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit
import SDWebImage

class PictureCommentsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var noCommentsLabel: UILabel!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    var picture: Picture?
    var comments: [Comment] = []
    var loading = false {
        didSet {
            self.updateUI()
        }
    }
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.commentsTableView.rowHeight = UITableViewAutomaticDimension
        self.commentsTableView.estimatedRowHeight = 95  // IB value
        
        self.configureRefreshControl()
        
        self.title = AppString(Comments)
        self.imageView.sd_setImageWithURL(
            NSURL(string: picture!.pictureURL!)!,
            placeholderImage: UIImage(named: "image-placeholder")
        )
        self.updateUI()
        
        self.loadComments()
    }
    
    // MARK: - UI
    
    private func updateUI() {
        self.loadingView.hidden = !self.loading
        if (self.comments.isEmpty && !self.loading) {
            self.noCommentsLabel.hidden = false
            self.noCommentsLabel.text = AppString(NoComments)
        } else {
            self.noCommentsLabel.hidden = true
        }
        self.commentsTableView.reloadData()
    }
    
    // MARK: - Refresh control
    
    let refreshControl = UIRefreshControl()
    
    private func configureRefreshControl() {
        self.refreshControl.tintColor = UIColor.lightGrayColor()
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        self.commentsTableView.addSubview(self.refreshControl)
    }
    
    func refresh(sender: UIRefreshControl) {
        self.refreshControl.endRefreshing()
        self.comments.removeAll()
        self.loadComments()
    }
    
    // MARK: - Table view
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(NSStringFromClass(CommentCell)) as! CommentCell
        
        if let comments = self.picture?.comments?.allObjects {
            if let comment = comments[indexPath.row] as? Comment {
                if let commentor = comment.user {
                    cell.commentorAvatarImageView.sd_setImageWithURL(
                        NSURL(string: commentor.profilePictureURL!)!,
                        placeholderImage: UIImage(named: "image-placeholder")
                    )
                    cell.commentorNameLabel.text = commentor.userName
                    cell.commentTextLabel.text = comment.text
                }
            }
        }
        
        return cell
    }
    
    // MARK: - Helpers
    
    private func loadComments() {
        InstagramManager.sharedManager.getComments(
            self.picture!,
            successed: { comments in
                dispatch_async(dispatch_get_main_queue()) {
                    self.comments = comments
                    self.loading = false
                }
            },
            failed: { error in
                dispatch_async(dispatch_get_main_queue()) {
                    ModalUtils.displayErrorAlert(
                        message: error.localizedDescription,
                        parentController: self
                    )
                    self.loading = false
                }
            }
        )
        self.loading = true
    }
}
