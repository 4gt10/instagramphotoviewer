//
//  UserMediaViewController.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit
import SDWebImage

class UserMediaViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var pictures: [Picture] = []
    var loading = false {
        didSet {
            self.collectionView.reloadSections(NSIndexSet(index: 0))
        }
    }
    var canLoadMore = true
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = SessionManager.sharedManager.session?.user?.userName ?? ""
        self.navigationItem.leftBarButtonItem =
            UIBarButtonItem(
                title: AppString(Logout),
                style: .Plain,
                target: self,
                action: "logout"
        )
        
        self.configureRefreshControl()
        self.registerForUserMediaNotifications()
        self.loadUserMedia(maxId: nil)
    }
    
    // MARK: - Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == SegueID.PicturesToPictureViewer) {
            if let pictureViewerVC = segue.destinationViewController as? PictureViewerViewController {
                pictureViewerVC.pictures = self.pictures
                pictureViewerVC.picture = sender as? Picture
                pictureViewerVC.canLoadMore = self.canLoadMore
            }
        }
    }
    
    // MARK: - Refresh control
    
    let refreshControl = UIRefreshControl()
    
    private func configureRefreshControl() {
        self.refreshControl.tintColor = UIColor.lightGrayColor()
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        self.collectionView.addSubview(self.refreshControl)
    }
    
    func refresh(sender: UIRefreshControl) {
        self.refreshControl.endRefreshing()
        self.pictures.removeAll()
        self.loadUserMedia(maxId: nil)
    }
    
    // MARK: - Collection view
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pictures.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let size = (collectionView.bounds.size.width / CGFloat(UIConstants.PicturesPerRow)) - UIConstants.SpaceBetweenPictures
        return CGSize(width: size, height: size)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return UIConstants.SpaceBetweenPictures
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return UIConstants.SpaceBetweenPictures
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(NSStringFromClass(PictureCell), forIndexPath: indexPath) as! PictureCell
        let picture = self.pictures[indexPath.item]
        
        cell.imageView.sd_setImageWithURL(
            NSURL(string: picture.thumbnailURL!)!,
            placeholderImage: UIImage(named: "image-placeholder")
        )
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableSupplementaryViewOfKind(
            UICollectionElementKindSectionFooter,
            withReuseIdentifier: NSStringFromClass(PicturesFooterView),
            forIndexPath: indexPath
        ) as! PicturesFooterView
        
        if (self.loading) {
            footerView.loadingView.hidden = false
            footerView.loadingView.startAnimating()
            footerView.picturesCountLabel.hidden = true
        } else {
            footerView.loadingView.hidden = true
            footerView.loadingView.stopAnimating()
            footerView.picturesCountLabel.hidden = false
            footerView.picturesCountLabel.text = self.pictures.isEmpty ? AppString(NoPictures) : "\(self.pictures.count) " + AppString(Pictures)
        }
        
        return footerView
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.item == self.pictures.count - 1) {
            if (!self.loading && self.canLoadMore) {
                let picture = self.pictures[indexPath.item]
                self.loadUserMedia(maxId: picture.id)
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let picture = self.pictures[indexPath.item]
        self.performSegueWithIdentifier(SegueID.PicturesToPictureViewer, sender: picture)
    }
    
    // MARK: - Notifications
    
    static var userMediaLoadedObserver: NSObjectProtocol?
    
    private func registerForUserMediaNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        
        if UserMediaViewController.userMediaLoadedObserver != nil {
            notificationCenter.removeObserver(UserMediaViewController.userMediaLoadedObserver!)
        }
        
        UserMediaViewController.userMediaLoadedObserver =
            notificationCenter.addObserverForName(
                NotificationCenter.UserMediaLoaded,
                object: nil,
                queue: NSOperationQueue.mainQueue()
                ) { notification in
                    if let canLoadMore = notification.userInfo?[NotificationKey.CanLoadMore] as? Bool {
                        self.canLoadMore = canLoadMore
                    }
                    if let userMedia = notification.userInfo?[NotificationKey.UserMedia] as? [Picture] {
                        self.pictures.appendContentsOf(userMedia)
                        self.collectionView.reloadSections(NSIndexSet(index: 0))
                    }
                }
    }
    
    // MARK: - Actions
    
    func logout() {
        SessionManager.sharedManager.logout()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Helpers
    
    private func loadUserMedia(maxId maxId: String?) {
        InstagramManager.sharedManager.getUserMedia(
            maxId: maxId,
            successed: { userMedia, canLoadMore in
                dispatch_async(dispatch_get_main_queue()) {
                    self.canLoadMore = canLoadMore
                    self.pictures.appendContentsOf(userMedia)
                    self.loading = false
                }
            },
            failed: { error in
                dispatch_async(dispatch_get_main_queue()) {
                    ModalUtils.displayErrorAlert(
                        message: error.localizedDescription,
                        parentController: self
                    )
                    self.loading = false
                }
            }
        )
        self.loading = true
    }
}
