//
//  Strings.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

import Foundation

func AppString(key: String) -> String {
    
    let StringsTableName = "Strings"
    let UnknownString = "???"
    
    let string = NSBundle.mainBundle().localizedStringForKey(
        key,
        value: UnknownString,
        table: StringsTableName
    )
    
    return string
}