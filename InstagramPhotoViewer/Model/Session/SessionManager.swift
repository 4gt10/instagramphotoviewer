//
//  Session.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit
import CoreData

class SessionManager {
    
    static let sharedManager = SessionManager()

    private let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var session: Session?
    
    // MARK: - Init
    
    private init() {
        let sessionRequest = NSFetchRequest(entityName: "Session")
        let sessions = try! self.managedObjectContext.executeFetchRequest(sessionRequest)
        if (!sessions.isEmpty) {
            self.session = sessions.first as? Session
        }
    }
    
    // MARK: - Interface
    
    func login(accessToken accessToken: String) {
        self.cleanSessions()
        self.session = NSEntityDescription.insertNewObjectForEntityForName(
            "Session",
            inManagedObjectContext: self.managedObjectContext
        ) as? Session
        self.session!.accessToken = accessToken
    }
    
    func logout() {
        self.cleanSessions()
        self.session = nil
        let storage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
        for cookie in storage.cookies! {
            storage.deleteCookie(cookie)
        }
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    // MARK: - Helpers
    
    func cleanSessions() {
        let sessionRequest = NSFetchRequest(entityName: "Session")
        let sessions = try! self.managedObjectContext.executeFetchRequest(sessionRequest)
        if (!sessions.isEmpty) {
            for session in sessions {
                self.managedObjectContext.deleteObject(session as! NSManagedObject)
            }
        }
    }
}