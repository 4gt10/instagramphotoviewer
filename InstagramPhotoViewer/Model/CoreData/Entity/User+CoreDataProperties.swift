//
//  User+CoreDataProperties.swift
//  
//
//  Created by 4gt10 on 30.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var userName: String?
    @NSManaged var id: String?
    @NSManaged var profilePictureURL: String?
    @NSManaged var pictures: NSSet?
    @NSManaged var comments: NSSet?
    @NSManaged var session: Session?

    func addPictures(pictures: [Picture]) {
        let mutablePictures = self.mutableSetValueForKey("pictures")
        mutablePictures.addObjectsFromArray(pictures)
    }
}
