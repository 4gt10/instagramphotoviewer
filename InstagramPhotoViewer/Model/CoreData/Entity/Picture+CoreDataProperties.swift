//
//  Picture+CoreDataProperties.swift
//  
//
//  Created by 4gt10 on 30.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Picture {

    @NSManaged var thumbnailURL: String?
    @NSManaged var pictureURL: String?
    @NSManaged var id: String?
    @NSManaged var user: User?
    @NSManaged var comments: NSSet?

    func addComment(comment: Comment) {
        let mutableComments = self.mutableSetValueForKey("comments")
        mutableComments.addObject(comment)
    }
}
