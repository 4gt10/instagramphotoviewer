//
//  Comment+CoreDataProperties.swift
//  
//
//  Created by 4gt10 on 30.01.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Comment {

    @NSManaged var text: String?
    @NSManaged var id: String?
    @NSManaged var picture: Picture?
    @NSManaged var user: User?

}
