//
//  InstagramManager.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData
import SDWebImage

struct InstagramURL {
    static let Base = "https://instagram.com/"
    static let Authorization = "https://instagram.com/oauth/authorize/?client_id=35827ac290374d098d6c4835e96e79fb&redirect_uri=https://www.instagram.com/&response_type=token"
    static let UserProfile = "https://api.instagram.com/v1/users/self/"
    static let UserMedia = "https://api.instagram.com/v1/users/self/media/recent/"
    static let Comments = "https://api.instagram.com/v1/media/{media-id}/comments/"
}

class InstagramManager {
    
    static let sharedManager = InstagramManager()
    
    private let alamofireManager = Alamofire.Manager(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    private let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // MARK: - Interface
    
    func getUserProfile(successed successed: (userProfile: User) -> Void, failed: (error: NSError) -> Void) {
        // Online
        if (NetworkUtils.isConnectedToNetwork()) {
            let accessToken = SessionManager.sharedManager.session?.accessToken ?? ""
            self.alamofireManager.request(
                .GET,
                InstagramURL.UserProfile,
                parameters: ["access_token" : accessToken],
                encoding: .URL,
                headers: nil
            )
            .response { _, _, response, error in
                if let data = response {
                    let jsonResponse = JSON(data: data)
                    let jsonUser = jsonResponse["data"]
                    if let cachedUser = self.cacheJSONUser(jsonUser, managedObjectContext: self.managedObjectContext) {
                        if let currentSession = SessionManager.sharedManager.session {
                            currentSession.user = cachedUser
                        }
                        successed(userProfile: cachedUser)
                    }
                } else if let error = error {
                    failed(error: error)
                } else {
                    let error = NSError(
                        domain: InstagramURL.UserProfile,
                        code: 0,
                        userInfo: [NSLocalizedDescriptionKey : AppString(DataErrorMessage)]
                    )
                    failed(error: error)
                }
            }
        // Offline
        } else {
            if let userProfile = SessionManager.sharedManager.session?.user {
                successed(userProfile: userProfile)
            } else {
                let error = NSError(
                    domain: InstagramURL.UserProfile,
                    code: 0,
                    userInfo: [NSLocalizedDescriptionKey : AppString(NoInternetConnection)]
                )
                failed(error: error)
            }
        }
    }
    
    func getUserMedia(maxId maxId: String?, successed: (userMedia: [Picture], canLoadMore: Bool) -> Void, failed: (error: NSError) -> Void) {
        // Online
        if (NetworkUtils.isConnectedToNetwork()) {
            let accessToken = SessionManager.sharedManager.session?.accessToken ?? ""
            var parameters = ["access_token" : accessToken]
            if let maxId = maxId {
                parameters["max_id"] = maxId
            } else {
                parameters["count"] = "\(UIConstants.PictiresPerPage)"
            }
            self.alamofireManager.request(
                .GET,
                InstagramURL.UserMedia,
                parameters: parameters,
                encoding: .URL,
                headers: nil
                )
                .response { _, _, response, error in
                    if let data = response {
                        let jsonResponse = JSON(data: data)
                        let jsonPictures = jsonResponse["data"]
                        var userFeed: [Picture] = []
                        for jsonPicture in jsonPictures.arrayValue {
                            if let cachedPicture = self.cacheJSONPicture(jsonPicture, managedObjectContext: self.managedObjectContext) {
                                userFeed.append(cachedPicture)
                            }
                        }
                        if let currentUser = SessionManager.sharedManager.session?.user {
                            currentUser.addPictures(userFeed)
                        }
                        let canLoadMore = !userFeed.isEmpty
                        successed(userMedia: userFeed, canLoadMore: canLoadMore)
                    } else if let error = error {
                        failed(error: error)
                    } else {
                        let error = NSError(
                            domain: InstagramURL.UserMedia,
                            code: 0,
                            userInfo: [NSLocalizedDescriptionKey : AppString(DataErrorMessage)]
                        )
                        failed(error: error)
                    }
            }
        // Offline
        } else {
            if let userMedia = SessionManager.sharedManager.session?.user?.pictures {
                successed(userMedia: userMedia.allObjects as! [Picture], canLoadMore: false)
            } else {
                let error = NSError(
                    domain: InstagramURL.UserMedia,
                    code: 0,
                    userInfo: [NSLocalizedDescriptionKey : AppString(NoInternetConnection)]
                )
                failed(error: error)
            }
        }
    }
    
    func getComments(picture: Picture, successed: (comments: [Comment]) -> Void, failed: (error: NSError) -> Void) {
        // Online
        if (NetworkUtils.isConnectedToNetwork()) {
            let commentsURL = InstagramURL.Comments.stringByReplacingOccurrencesOfString(
                "{media-id}",
                withString: picture.id ?? ""
            )
            let accessToken = SessionManager.sharedManager.session?.accessToken ?? ""
            self.alamofireManager.request(
                .GET,
                commentsURL,
                parameters: ["access_token" : accessToken],
                encoding: .URL,
                headers: nil
            )
            .response { _, _, response, error in
                if let data = response {
                    let jsonResponse = JSON(data: data)
                    let jsonComments = jsonResponse["data"]
                    var comments: [Comment] = []
                    for jsonComment in jsonComments.arrayValue {
                        if let cachedComment = self.cacheJSONComment(jsonComment, managedObjectContext: self.managedObjectContext) {
                            picture.addComment(cachedComment)
                            comments.append(cachedComment)
                        }
                    }
                    successed(comments: comments)
                } else if let error = error {
                    failed(error: error)
                } else {
                    let error = NSError(
                        domain: InstagramURL.UserMedia,
                        code: 0,
                        userInfo: [NSLocalizedDescriptionKey : AppString(DataErrorMessage)]
                    )
                    failed(error: error)
                }
            }
        // Offline
        } else {
            if let comments = picture.comments {
                successed(comments: comments.allObjects as! [Comment])
            } else {
                let error = NSError(
                    domain: InstagramURL.UserMedia,
                    code: 0,
                    userInfo: [NSLocalizedDescriptionKey : AppString(NoInternetConnection)]
                )
                failed(error: error)
            }
        }
    }
    
    // MARK: - Helpers
    
    private func cacheJSONPicture(jsonPicture: JSON, managedObjectContext: NSManagedObjectContext) -> Picture? {
        var result: Picture?
        
        if (jsonPicture["type"].stringValue == "image") {
            let pictureFetchRequest = NSFetchRequest(entityName:"Picture")
            let picturePredicate = NSPredicate(format: "id = %@", jsonPicture["id"].stringValue)
            pictureFetchRequest.predicate = picturePredicate
            let pictureFetchResults = try! managedObjectContext.executeFetchRequest(pictureFetchRequest)
            if (pictureFetchResults.isEmpty) {
                let picture = NSEntityDescription.insertNewObjectForEntityForName(
                    "Picture",
                    inManagedObjectContext: managedObjectContext
                    ) as! Picture
                // ID
                picture.id = jsonPicture["id"].stringValue
                
                // Thumbnail
                picture.thumbnailURL = jsonPicture["images"]["thumbnail"]["url"].stringValue
                SDWebImageManager.sharedManager().downloadImageWithURL(
                    NSURL(string: picture.thumbnailURL!)!,
                    options: .ContinueInBackground,
                    progress: { _, _ in },
                    completed: { _, _, _, _, _ in }
                )
                
                // Picture
                picture.pictureURL = jsonPicture["images"]["standard_resolution"]["url"].stringValue
                SDWebImageManager.sharedManager().downloadImageWithURL(
                    NSURL(string: picture.pictureURL!)!,
                    options: .ContinueInBackground,
                    progress: { _, _ in },
                    completed: { _, _, _, _, _ in }
                )
                
                // User
                if let cachedUser = self.cacheJSONUser(jsonPicture["user"], managedObjectContext: managedObjectContext) {
                    picture.user = cachedUser
                }
                
                result = picture
            } else {
                result = pictureFetchResults.first as? Picture
            }
        }
        
        return result
    }
    
    private func cacheJSONUser(jsonUser: JSON, managedObjectContext: NSManagedObjectContext) -> User? {
        var result: User?
        
        let userFetchRequest = NSFetchRequest(entityName: "User")
        let userPredicate = NSPredicate(format: "id = %@", jsonUser["id"].stringValue)
        userFetchRequest.predicate = userPredicate
        let userFetchResults = try! managedObjectContext.executeFetchRequest(userFetchRequest)
        if (userFetchResults.isEmpty) {
            let user = NSEntityDescription.insertNewObjectForEntityForName(
                "User",
                inManagedObjectContext: managedObjectContext
                ) as! User
            // ID
            user.id = jsonUser["id"].stringValue
            
            // Name
            user.userName = jsonUser["username"].stringValue
            
            // Picture
            user.profilePictureURL = jsonUser["profile_picture"].stringValue
            SDWebImageManager.sharedManager().downloadImageWithURL(
                NSURL(string: user.profilePictureURL!)!,
                options: .ContinueInBackground,
                progress: { _, _ in },
                completed: { _, _, _, _, _ in }
            )
            
            result = user
        } else {
            result = userFetchResults.first as? User
        }
        
        return result
    }
    
    private func cacheJSONComment(jsonComment: JSON, managedObjectContext: NSManagedObjectContext) -> Comment? {
        var result: Comment?
        
        let commentFetchRequest = NSFetchRequest(entityName: "Comment")
        let commentPredicate = NSPredicate(format: "id = %@", jsonComment["id"].stringValue)
        commentFetchRequest.predicate = commentPredicate
        let commentFetchResults = try! managedObjectContext.executeFetchRequest(commentFetchRequest)
        if (commentFetchResults.isEmpty) {
            let comment = NSEntityDescription.insertNewObjectForEntityForName(
                "Comment",
                inManagedObjectContext: managedObjectContext
                ) as! Comment
            // ID
            comment.id = jsonComment["id"].stringValue
            
            // Text
            comment.text = jsonComment["text"].stringValue
            
            // Commentor
            let jsonCommentorUser = jsonComment["from"]
            if let cachedCommentorUser = self.cacheJSONUser(jsonCommentorUser, managedObjectContext: managedObjectContext) {
                comment.user = cachedCommentorUser
            }
            
            result = comment
        } else {
            result = commentFetchResults.first as? Comment
        }
        
        return result
    }
}