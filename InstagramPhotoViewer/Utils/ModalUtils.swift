//
//  ModalUtils.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class ModalUtils {
    
    class func displayErrorAlert(message message: String?, parentController: UIViewController) {
        let alert = UIAlertController(
            title: AppString(Error),
            message: message,
            preferredStyle: .Alert
        )
        alert.addAction(UIAlertAction(title: AppString(OK), style: .Cancel, handler: nil))
        parentController.presentViewController(alert, animated: true, completion: nil)
    }
    
    class func configureNetworkActivityIndicator(visible visible: Bool) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = visible
    }
}