//
//  PicturesFooterView.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class PicturesFooterView: UICollectionReusableView {
        
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var picturesCountLabel: UILabel!
    
}
