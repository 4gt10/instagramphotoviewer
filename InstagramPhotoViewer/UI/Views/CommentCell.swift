//
//  CommentCell.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 31.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var commentorAvatarImageView: UIImageView!
    @IBOutlet weak var commentorNameLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    
}
