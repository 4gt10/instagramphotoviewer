//
//  StringsConstants.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

let InstagramPhotoViewer = "InstagramPhotoViewer"
let Login = "Login"
let Error = "Error"
let OK = "OK"
let DataErrorMessage = "DataErrorMessage"
let Pictures = "Pictures"
let NoPictures = "NoPictures"
let Logout = "Logout"
let NoInternetConnection = "NoInternetConnection"
let Of = "Of"
let Loading = "Loading"
let NoPhoto = "NoPhoto"
let Comments = "Comments"
let NoComments = "NoComments"