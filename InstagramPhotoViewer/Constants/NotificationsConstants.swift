//
//  NotificationsConstants.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 31.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

struct NotificationKey {
    static let UserMedia = "userMedia"
    static let CanLoadMore = "canLoadMore"
}

struct NotificationCenter {
    static let UserMediaLoaded = "InstagramPhotoViewer.UserMediaLoaded"
}