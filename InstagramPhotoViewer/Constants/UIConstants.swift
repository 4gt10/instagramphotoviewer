//
//  UIConstants.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

struct UIConstants {
    static let PicturesPerRow = 2
    static let PictiresPerPage = 10
    static let SpaceBetweenPictures: CGFloat = 3
}