//
//  StoryboardConstants.swift
//  InstagramPhotoViewer
//
//  Created by 4gt10 on 30.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

struct ViewControllerID {
    static let LoginVC = "LoginViewController"
    static let UserMediaVC = "UserMediaViewController"
    static let PictureViewerVC = "PictureViewerViewController"
    static let PictureVC = "PictureViewController"
    static let PictureCommentsVC = "PictureCommentsViewController"
}

struct SegueID {
    static let LoginToUserMedia = "LoginToUserMediaSegue"
    static let PicturesToPictureViewer = "PicturesToPictureViewerSegue"
    static let PictureViewerToComments = "PictureViewerToCommentsSegue"
}